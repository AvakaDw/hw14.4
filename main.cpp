#include <iostream>

int main() {
	std::string DName;
	std::cout << "Enter your word: ";
	std::cin >> DName;
	std::cout << "Your string: " << DName << std::endl;
	std::cout << "String length: " << DName.size() << std::endl;
	std::cout << "First character: " << DName.front() << std::endl;
	std::cout << "Last character: " << DName.back() << std::endl;
}